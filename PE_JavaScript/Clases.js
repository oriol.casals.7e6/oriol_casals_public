//CLASES
class Usuario {
    constructor(nombre, apellidos, direccion, cuenta) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.cuenta = cuenta;
    }

    codigoHTML() {
        return "<p>"+this.nombre+" " + this.apellidos +"</p><p>"+this.direccion+"</p>";
    }
}

class Articulo {
    constructor(nombre, precio, cantidad, descuento) {
        this.nombre = nombre;
        this.precio = precio;
        this.cantidad = cantidad;
        this.descuento = descuento;
    }
    get clave() {
        return this.nombre;
    }

    get precioDescuento(){
        return this.precio*(this.descuento/100);
    }

    get precioTotal(){
        return (this.precio-this.precioDescuento) * this.cantidad
    }
}


class CarroCompra {
    #articulos
    constructor() {
        this.#articulos = new Map();
    }

    addArticulo(articulo) {
        if (this.#articulos.has(articulo.clave)) {
            let art = this.#articulos.get(articulo.clave);
            art.cantidad = parseInt(art.cantidad) + parseInt(articulo.cantidad);
        } else {
            this.#articulos.set(articulo.clave, articulo);
        }
    }

    getArticulos() {
        console.log(this.#articulos.values());
        return Array.from(this.#articulos.values());
    }

    setArticulos(compra) {
        console.log(compra);
        for (let aC of compra) {
            let art = new Articulo(aC.nombre, aC.precio, aC.cantidad, aC.descuento)
            this.addArticulo(art);
        }
    }
}