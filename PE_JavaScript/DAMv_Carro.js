let productos = [{
    nombre: "Milkybar",
    foto: "milkybar.jpg",
    descripcion: "Chocolate blanco Nestlé Milkybar 100 g.",
    precio: 1.20,
    descuento: "10%",
}, {
    nombre: "Crunch",
    foto: "crunch.jpg",
    descripcion: "Chocolate crujiente Crunch Nestlé sin gluten 100 g.",
    precio: 1.32,
    descuento: "20%",
}, {
    nombre: "Croissants - La Bella Easo",
    foto: "croissant.jpg",
    descripcion: "Croissants 0% azucares La Bella Easo 360 g.",
    precio: 2.59,
    descuento: "",
}, {
    nombre: "Nesquik",
    foto: "nesquik.jpg",
    descripcion: "Chocolate con leche con relleno cremoso Nestlé Nesquik 100 g.",
    precio: 1.31,
    descuento: "30%",
}];

let user;
let carro;


function init() {
    recuperaDatosUsuario();
    muestraProductos();

    let myButton = document.createElement("button");
    myButton.innerText = "Añadir Productos";
    myButton.id = "anadirProds";

    let section = document.getElementsByTagName("section");
    let mySection = section[0];
    mySection.appendChild(myButton)

    var carro = new CarroCompra();

    let compra = [];

    for (const prod of productos) {
        var articulo = {
            nombre: prod.nombre,
            precio: prod.precio,
            cantidad: document.getElementById(prod.nombre),
            descuento: prod.descuento
        }
        compra.push(articulo);
        carro.addArticulo(articulo);
    }

    carro.setArticulos(compra);

    let button = document.getElementById("anadirProds");
    button.addEventListener("click", () => anadirProductos());
}


function recuperaDatosUsuario() {
    /* Función para mostrar el modal de configuración si es la primera vez que se visita la página*/

    let myForm = document.createElement("form");
    myForm.id = "myForm";

    let myWindow = window.open('', '', 'width=400, height=400');
    myWindow.document.write('<h1>Datos del usuario</h1> <p>nombre: <input type="text" name="nombre" id="Nombre"></p> <p>apellidos: <input type="text" name="apellidos" id="Apellidos"></p> <p>direccion: <input type="text" name="direccion" id="Direccion"></p> ');
    myWindow.document.write('<button id="cookies">Guardar</button>');

    document.cookie = "firstTime=True";

    // let button = document.getElementById("cookies");
    // button.addEventListener("click", () => 
    // document.cookie = "firstTime=True",
    // document.cookie = "nombre=True",
    // document.cookie = "firstTime=True",

    // );

    

    var myCookies = document.cookie;
    console.log(myCookies);
    var cookieArray = myCookies.split(";")
    console.log(cookieArray);

    // if (checkFirstTime(cookieArray)) {
    //     var myWindow = window.open("", "", "width=200,height=100");
    // }


    function checkFirstTime(cookieArray) {
        let firstTimeIndex = cookieArray.findIndex((item) => {
            item == "firstTime";
        });
        console.log(cookieArray[firstTimeIndex + 1]);
        return (cookieArray[firstTimeIndex + 1].split("=") == true);
    }
}

function muestraProductos() {
    /** Función para crear los elementos necesarios para poder visualziar los productos. */
    for (const producto of productos) {

        var divProds = document.getElementById("productos");

        var article = document.createElement("article");
        article.classList.add("articulo");
        divProds.appendChild(article);

        var div = document.createElement("div");
        article.appendChild(div);

        var nombreProducto = document.createElement("p");
        nombreProducto.innerText = producto.nombre;
        nombreProducto.classList.add("w50");
        div.appendChild(nombreProducto);

        var descuento = document.createElement("p");
        descuento.innerText = producto.descuento;
        descuento.classList.add("descuento");
        div.appendChild(descuento);

        var img = document.createElement("img");
        img.src = "src/" + producto.foto;
        article.appendChild(img);

        var precio = document.createElement("p");
        precio.innerText = producto.precio;
        article.appendChild(precio);

        var descripcion = document.createElement("p");
        descripcion.innerText = producto.descripcion;
        article.appendChild(descripcion)

        var inputContainer = document.createElement("p");
        var inputProducto = document.createElement("input");
        inputProducto.type = "number";
        inputProducto.id = producto.nombre;
        inputProducto.step = "1";
        inputProducto.min = "0";
        article.appendChild(inputContainer);
        inputContainer.appendChild(inputProducto)


    }

}



function anadirProductos() {
    // let products = document.getElementsByClassName("articulo");

    // 

    // for (const prods of products) {
    //     console.log(prods.getElementsByClassName("w50"));
    //     let tr = document.createElement("tr");
    //     table.appendChild(tr);

    //     let articulo = document.createElement("td");
    //     articulo.innerText = prods.id;

    //     let precio = document.createElement("td");
    //     precio.innerText = prods.precio;

    //     let descuento = document.createElement("td");
    //     descuento.innerText = prods
    //     let cantidad = document.createElement("td");
    //     let total = document.createElement("td");
    // }
    
    let table = document.getElementById("tablebody");

    for (let index = 0; index < carro.getArticulos().length; index++) {
        let tr = document.createElement("tr");
        table.appendChild(tr);

        for (const value of carro.getArticulos()[index]) {
            let td = document.createElement('td');
            td.innerText = value;

            tr.appendChild(td);
        }
    }
}


function creaCelda(valor) {
    let celda = document.createElement("td");
    celda.innerText = valor;
    return celda;
}
let button = document.getElementById("anadirProds");
function vaciaCarro() {
    let tabla = document.getElementById("tablebody");
    while (tabla.hasChildNodes()) {
        let child = tabla.removeChild(tabla.firstChild)

    }

}



