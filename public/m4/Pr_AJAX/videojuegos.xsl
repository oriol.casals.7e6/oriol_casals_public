<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/videojuegos">
        <body>
            <main>
                <h2>VIDEOJUEGOS</h2>
                <div class="videojocs">
                    <xsl:for-each select="videojuego">
                        <article>
                            <div class="img">
                                <img>
                                    <xsl:attribute name="src">
                                        <xsl:value-of select="infoGeneral/foto"/>
                                    </xsl:attribute>
                                </img>
                            </div>
                            <h3>
                                <xsl:value-of select="nombre"/>
                            </h3>
                            <div class="txt">
                                <p>
                                    <xsl:value-of select="infoGeneral/descripcion"/>
                                </p>
                            </div>
                            <div class="boto">
                                <a>
                                    <xsl:attribute name="href">
                                        <xsl:value-of select="@idVideojuego"/>
                                    </xsl:attribute>
                            más info
                                </a>
                            </div>
                        </article>
                    </xsl:for-each>
                </div>
            </main>
        </body>
    </xsl:template>
</xsl:stylesheet>