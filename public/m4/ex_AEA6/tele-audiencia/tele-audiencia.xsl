<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
        <cadena>
            <nom>Un TV</nom>
            <programas>
                <xsl:for-each select="programacio/audiencia">
                    <programa>
                        <xsl:attribute name="hora">
                            <xsl:value-of select="hora"/>
                        </xsl:attribute>
                        <xsl:element name="nom-programa">
                            <xsl:value-of select="cadenes/cadena[@nom='Un TV']"/>
                        </xsl:element>
                        <xsl:element name="audiencia">
                            <xsl:value-of select="cadenes/cadena[@nom='Un TV']/@percentatge"/>
                        </xsl:element>
                    </programa>
                </xsl:for-each>
            </programas>
        </cadena>
    </xsl:template>
</xsl:stylesheet>