<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" indent="yes"/>
        <xsl:template match="/">
            <html>
                <body>
                    <table border="1">
                        <tr>
                            <th>fecha</th>
                            <th>maxima</th>
                            <th>minima</th>
                            <th>prediccion</th>
                        </tr>
                            <xsl:for-each select="root/prediccion/dia">
                                <tr>
                                    <td><xsl:value-of select="./@fecha"/></td>
                                    <td><xsl:value-of select="./temperatura/maxima"/></td>
                                    <td><xsl:value-of select="./temperatura/minima"/></td>
                                    <td><img src="{concat('imagenes/',estado_cielo[@periodo='00-24']/@descripcion)}.png" style="width:50px"/></td>
                                </tr>
                            </xsl:for-each>
                    </table>
                </body>
            </html>
        </xsl:template>
</xsl:stylesheet>
