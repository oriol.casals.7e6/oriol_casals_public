<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" indent="yes"/>
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="/">
        <html>
            <body>
                <table border="1">
                    <tr style="background-color:lightgreen">
                        <th>foto</th>
                        <th>nombre</th>
                        <th>apellidos</th>
                        <th>telefono</th>
                        <th>repetidor</th>
                        <th>nota practica</th>
                        <th>nota examen</th>
                        <th>nota total</th>
                    </tr>
                    <xsl:for-each select="evaluacion/alumno">
                        <xsl:sort order ="descending"/>
                        <tr>
                            <td>
                                <!-- fer choose i when per afegir foto concreta o general quan faci falta -->
                                <img width="100px" height="100px">
                                    <xsl:attribute name="src">
                                        <xsl:value-of select="img"/>
                                    </xsl:attribute>
                                </img>
                            </td>
                            <td>
                                <xsl:value-of select="nombre"/>
                            </td>
                            <td>
                                <xsl:value-of select="apellidos"/>
                            </td>
                            <td>
                                <xsl:value-of select="telefono"/>
                            </td>
                            <td>
                                <xsl:value-of select="@repite"/>
                            </td>
                            <td>
                                <xsl:value-of select="notas/practicas"/>
                            </td>
                            <td>
                                <xsl:value-of select="notas/examen"/>
                            </td>
                            <xsl:apply-templates select="notas"/>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="notas">
        <xsl:choose>
            <xsl:when test="(practicas + examen) div 2 &lt; 5">
                <td style="background-color:red">
                    <xsl:value-of select="(practicas + examen) div 2"/>
                </td>
            </xsl:when>
            <xsl:when test="(practicas + examen) div 2 &gt;= 8">
                <td style="background-color:lightblue">
                    <xsl:value-of select="(practicas + examen) div 2"/>
                </td>
            </xsl:when>
            <xsl:otherwise>
                <td>
                    <xsl:value-of select="(practicas + examen) div 2"/>
                </td>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>