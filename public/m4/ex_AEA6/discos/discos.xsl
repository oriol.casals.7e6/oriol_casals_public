<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="/">
        <xsl:element name="lista">
            <xsl:apply-templates select="discos/group"></xsl:apply-templates>
        </xsl:element>
    </xsl:template>

    <xsl:template match="group">

        <xsl:variable name="nombre" select="name"></xsl:variable>
        <xsl:variable name="idGrupo" select="@id"></xsl:variable>
        <xsl:for-each select="//disco[interpreter/@id=$idGrupo]">
            <xsl:element name="disco">
                <xsl:if test="@id=$idGrupo">
                    <xsl:value-of select="title"/>
        es iterpretado por:
                    <xsl:value-of select="$nombre"/>
                </xsl:if>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
