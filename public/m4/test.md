# Títol
## titol 2
### titol 3
#### titol 4

doble enter = salt de línea

titol
===
titol 2
___

**negrita**
__negrita__

*cursiva* 
_cursiva_

***curs i neg***
___curs i neg___

- elem
+ elem
* elem
    + elem
    * elem
        + elem
* elem

1. elem
1. elem
1. elem
    1. elem
    1. elem
        1. elem
1. elem

~~~ kt
println("hola")
~~~

```html
<html>
    <head></head>
    <body></body>
</html>
```
[enllaç](https://oriolcasals-oriolcasals-f6sgocxppt4.ws-eu74.gitpod.io/)

[enllaç a git][git]

[git]://oriolcasals-oriolcasals-f6sgocxppt4.ws-eu74.gitpod.io/

https://oriolcasals-oriolcasals-f6sgocxppt4.ws-eu74.gitpod.io/

![img](/public/Pr%C3%A0ctica/src/chad.jpeg)

| usuari | correu | ciutat |
|--------|--------|--------|
|Juan|juan@gmail.com|albacete|
