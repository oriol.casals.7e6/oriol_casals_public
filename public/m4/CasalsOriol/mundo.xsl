<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    

    <xsl:template match="/">
        <xsl:apply-templates select="mundo"></xsl:apply-templates>
    </xsl:template>
    

    <xsl:template match="mundo">
        <xsl:for-each select="continente">
            <h1><xsl:value-of select="nombre"/></h1>
            <table border="1">
                <xsl:attribute name="style">border-color:<xsl:value-of select="nombre/@color"></xsl:value-of></xsl:attribute>
                <tr>
                    <th>Bandera</th>
                    <th>País</th>
                    <th>Gobierno</th>
                    <th>Capital</th>
                </tr>
                <xsl:apply-templates select="paises"></xsl:apply-templates>
            </table>
        </xsl:for-each>
    </xsl:template>
    

    <xsl:template match="paises">
        <xsl:for-each select="pais">
            <xsl:sort order="ascending"></xsl:sort>
            <tr>
                <td>
                    <img src="img/{foto}"/>
                </td>
                <td><xsl:value-of select="nombre"/></td>
                <xsl:apply-templates select="nombre"></xsl:apply-templates>
                <td><xsl:value-of select="capital"/></td>
            </tr>
        </xsl:for-each>
    </xsl:template>


    <xsl:template match="nombre">
        <xsl:choose>
            <xsl:when test="@gobierno = 'dictadura'">
                <td style="background-color:red">
                    <xsl:value-of select="@gobierno"/>
                </td>
            </xsl:when>
            <xsl:when test="@gobierno = 'monarquia'">
                <td style="background-color:yellow">
                    <xsl:value-of select="@gobierno"/>
                </td>
            </xsl:when>
            <xsl:otherwise>
                <td><xsl:value-of select="@gobierno"/></td>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    
</xsl:stylesheet>
