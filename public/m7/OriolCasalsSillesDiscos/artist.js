class Artist {
    #name;
    #surnames;
    #artisticName;

    constructor() {
        this.#name = "";
        this.#surnames = "";
        this.#artisticName = "";
    };

    get name() { return this.#name };
    set name(newName) { this.#name = newName };

    get surnames() { return this.#surnames };
    set surnames(newSurnames) { this.#surnames = newSurnames };

    get artisticName() { return this.#artisticName };
    set artisticName(newArtisticName) { this.#artisticName = newArtisticName };
}