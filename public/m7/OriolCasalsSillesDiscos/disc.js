class Disco {
    #location;
    #borrowed;

    constructor() {
        this.discName = "";
        this.artist = {
            name: "",
            surname: "",
            artisticName: ""
        };
        this.year = "";
        this.genre = "";
        this.discCover = "";
        this.#location = 0;
        this.#borrowed = false;
    }

    get location() { return this.location };
    set location(newLocation) { this.location = newLocation };

    get borrowed() { return this.#borrowed };
    set borrowed(newBorrowed) { this.borrowed = newBorrowed };

    setDiscProperties() {
        alert("Lets save a new disc");
        this.discName = prompt("Disc Name");
        var artist = {
            name: prompt("name"),
            surnames: prompt("surnames"),
            artisticName: prompt("artistic name")
        }
        this.artist = artist;
        this.year = prompt("Year");
        this.genre = prompt("Genre");
        this.discCover = prompt("Link to disc cover");
    };

    discInfo() { 
        return this;
    };
}






