class Dicography {
    allDicographyDiscs;
    constructor() {
        this.allDicographyDiscs = [];
    }

    addDisc() {
        var newDisc = new Disco();
        newDisc.setDiscProperties();
        this.allDicographyDiscs.push(newDisc);
        for (let i = 0; i < this.allDicographyDiscs; i++) {
            if (allDicographyDiscs[i] == newDisc) {
                newDisc.location(allDicographyDiscs[i]);
            }
        }
    };

    removeDisc(discToRemove) {
        let indexToRemove = this.allDicographyDiscs.findIndex(disc => disc.discName == discToRemove);
        console.log(indexToRemove);
        this.allDicographyDiscs.splice(indexToRemove, 1);
    };

    sortDiscs(discAtribute) {
        this.allDicographyDiscs.sort((a, b) => (a.discAtribute > b.color) ? 1 : -1);
    };

    dicographyInfo(){
        return this.allDicographyDiscs();
    }
}