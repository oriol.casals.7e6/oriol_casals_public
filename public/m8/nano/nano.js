class elNano {
    image;
    positionX;
    positionY;

    constructor() {
        this.image = "pngwing.com.png";
        this.positionX = 200;
        this.positionY = 150;
    }

    generateElNano() { //creem una  imatge del cotxe i el pintem per pantalla
        // var div = document.createElement("div");
        // div.setAttribute("id", "elNano");
        var img = document.createElement("img");
        img.setAttribute("src", this.image);
        img.setAttribute("id", "elNano");
        // div.appendChild(img);
        document.body.appendChild(img);
    }

    explodeNano(event){
        var nano = document.getElementById('elNano');
        nano.src = "exposion.png";
        nano.style.top = event.clientY - this.positionY + 100 + "px";
        var timeoutId = setTimeout(() => nano.src = "pngwing.com.png", 1000);
        
    }
}

class road {
    image;

    constructor() {
        this.image = "road.avif"
    }

    generateRoad() {
        var img = document.createElement("img");
        img.setAttribute("src", this.image);
        img.setAttribute("id", "road");
        document.body.appendChild(img);
        // document.body.style.backgroundImage = this.image;
    }
}

class enemy {
    image;

    constructor() {
        this.image = "";
    }

    generateEnemyCar() {
        var img = document.createElement("img");
        img.setAttribute("src", this.image);
        img.setAttribute("id", "enemy");
        document.body.appendChild(img);
    }
}


function game() {

    //instanciem els objectes del joc
    var nanoCar = new elNano();
    nanoCar.generateElNano();

    var roadTest = new road();
    roadTest.generateRoad();

    var enemyCar = new enemy();
    enemyCar.generateEnemyCar();
    

    document.addEventListener('mousemove', function (event) { //apliquem el seguiment del mouse
        var nano = document.getElementById('elNano');
        var road = document.getElementById('road');
        var car = document.getElementById('enemy');

        var x = nanoCar.positionX;
        var y = event.clientY - nanoCar.positionY;
        nano.style.position = "absolute";
        nano.style.left = x + "px";
        nano.style.top = y + "px";

        if (checkTrackLimits(nano.getBoundingClientRect(), road.getBoundingClientRect())) {
            nanoCar.explodeNano(event);
        }

        var intervalID = setInterval(() => {
            car.src = "car.png";
            car.style.position = "absolute";
            car.style.top = 200 + 'px';
            car.style.right = 50 + 'px';
            clearInterval(intervalID);
        }, 5000);
    });
}
game();


function checkTrackLimits(nanoInfo, roadInfo) {
    return (nanoInfo.bottom >= (roadInfo.bottom) || nanoInfo.top <= (roadInfo.top));
}